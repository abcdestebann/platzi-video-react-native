import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import AppNavigator from './app/src/app-navigator'

import Home from './app/src/screens/Home/Home';
import Movie from './app/src/screens/Movie/Movie';
import LoadingLogo from './app/src/components/LoadingLogo/LoadingLogo'

import api from './app/src/api/index'

import createStore from './app/src/store'
import { setCategoriesToState, setSuggesitonsToState } from './app/src/actions/index';

console.disableYellowBox = true

const { store, persistor } = createStore()

class App extends Component {

  async componentDidMount() {
    api.getSuggestions(10)
      .then((suggestions) => store.dispatch(setCategoriesToState(suggestions.movies)))
      .catch((error) => console.error(error))
    api.getCategories()
      .then((categories) => store.dispatch(setSuggesitonsToState(categories.movies)))
      .catch((error) => console.error(error))
  }

  render() {

    return (
      <Provider store={store}>
        <PersistGate loading={<LoadingLogo />} persistor={persistor}>
          <ScrollView>
            <AppNavigator />
            <Movie />
            <Home />
          </ScrollView>
        </PersistGate>
      </Provider>
    );
  }
}


export default App
