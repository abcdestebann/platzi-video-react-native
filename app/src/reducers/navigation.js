import AppNavigator from '../app-navigator';
import { createNavigationReducer } from 'react-navigation-redux-helpers'

export default createNavigationReducer(AppNavigator)