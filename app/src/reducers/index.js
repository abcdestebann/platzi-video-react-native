import { combineReducers } from 'redux';
import Navigation from './navigation';
import Main from './main';

export default combineReducers({
   Navigation,
   Main
})