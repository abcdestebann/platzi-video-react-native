import {
   SET_DATA_CATEGORIES,
   SET_DATA_SUGGESTIONS,
   SET_MOVIE_SELECTED
} from '../actions/constants';

/**
 *
 *
 * @export
 * @param {*} state
 * @param {*} action
 * @returns
 */
export default function mainReducer(state = {}, action) {
   console.log(action)
   switch (action.type) {
      case SET_DATA_CATEGORIES:
         // newState.categories = action.payload
         // newState
         return { ...state, ...action.payload }
      case SET_DATA_SUGGESTIONS:
         // newState.suggestions = action.payload
         // return newState
         return { ...state, ...action.payload }
      case SET_MOVIE_SELECTED:
         // newState.movie = action.payload
         // return newState
         return { ...state, ...action.payload }
      default: return state
   }
}