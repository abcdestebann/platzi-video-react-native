import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    separator: {
        borderBottomWidth: 1,
        borderBottomColor: '#eaeaea',
    }
})