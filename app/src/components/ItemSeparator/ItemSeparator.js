import React from 'react';
import { View } from 'react-native';
import styles from './styles';

const ItemSeparator = () => (
    <View style={styles.separator}>

    </View>
);

export default ItemSeparator;
