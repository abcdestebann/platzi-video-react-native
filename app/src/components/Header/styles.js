import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 2,
        paddingVertical: 5,
        borderBottomColor: '#efefef',
    },
    logo: {
        width: 100,
        height: 30,
        resizeMode: 'cover',
        margin: 10
    },
    children: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
    }
})