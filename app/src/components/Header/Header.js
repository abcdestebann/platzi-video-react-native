import React from 'react'
import { View, SafeAreaView, Image } from 'react-native';
import styles from './styles'

const Header = (props) => {
	return (
		<View>
			<SafeAreaView style={styles.container}>
				<View>
					<Image
						source={require('../../../assets/logo.png')}
						style={styles.logo} />
				</View>
				<View style={styles.children}>
					{props.children}
				</View>
			</SafeAreaView>
		</View>
	)
}

export default Header
