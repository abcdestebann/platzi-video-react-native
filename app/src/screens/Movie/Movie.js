import React, { Component } from 'react';
import { View, TouchableOpacity, Animated, ScrollView } from 'react-native';

import { connect } from 'react-redux'
import { setMovieToState } from '../../actions/index'

import LayoutMovie from './components/LayoutMovie/LayoutMovie';
import Player from './containers/Player/Player'
import Header from '../../components/Header/Header';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DetailsMovie from './components/DetailsMovie/DetailsMovie'

class Movie extends Component {

   state = {
      opacity: new Animated.Value(0),
   }

   componentDidMount() {
      Animated.timing(                  // Animate over time
         this.state.opacity,            // The animated value to drive
         {
            toValue: 1,                   // Animate to opacity: 1 (opaque)
            duration: 2000,              // Make it take a while
         }
      ).start();
   }

   hideMovie = () => this.props.setMovieToState({})

   render() {
      let { opacity } = this.state;
      if (this.props.movie && Object.keys(this.props.movie).length > 0) {
         return (
            <ScrollView>
               <LayoutMovie>
                  <Animated.View style={{
                     opacity: opacity,
                  }}>
                     <Header>
                        <TouchableOpacity hitSlop={{
                           top: 5,
                           left: 5,
                           right: 5,
                           bottom: 5
                        }} onPress={this.hideMovie} >
                           <Icon name="close" size={30} color="#e87a7a" />
                        </TouchableOpacity>
                     </Header>
                     <Player />
                     <DetailsMovie {...this.props.movie} />
                  </Animated.View>
               </LayoutMovie>
            </ScrollView>
         );
      }
      return (<View></View>)
   }
}

const mapStateToProps = (state) => {
   return {
      movie: state.movie
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
      setMovieToState: (data) => dispatch(setMovieToState(data))
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(Movie) 