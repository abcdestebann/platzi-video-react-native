import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    container: {
        height: 200,
        width: '100%'
    },
    video: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    progress: {
        height: 5,
        width: '100%'
    }
})