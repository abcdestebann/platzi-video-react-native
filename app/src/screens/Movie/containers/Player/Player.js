import React, { Component } from 'react'
import { ActivityIndicator, Text, ProgressViewIOS } from 'react-native'
import Video from 'react-native-video';

import LayoutVideo from '../../components/LayoutVideo/LayoutVideo';
import ControlsLayout from '../../components/ControlsLayout/ControlsLayout';
import PlayPause from '../../components/PlayPause/PlayPause';
import Fullscreen from '../../components/Fullscreen/Fullscreen';
import Volume from '../../components/Volume/Volume';
import TimeVideo from '../../components/TimeVideo/TimeVideo'
import ProgressBar from '../../components/ProgressBar/ProgressBar'


import styles from './styles'

import { formattedTime } from '../../../../utils/index'

export default class Player extends Component {

	state = {
		isLoading: true,
		paused: false,
		muted: false,
		duration: 0,
		progress: 0,
		currentTime: 0,
		showPlayPause: false
	}

	onBuffer = ({ isBuffering }) => this.setState({ isLoading: isBuffering })

	onLoad = (event) => {
		this.setState({
			isLoading: false,
			duration: event.duration,
		})
	}

	setProgress = (event) => {
		this.setState({
			currentTime: event.currentTime,
			progress: (event.currentTime / event.seekableDuration)
		})
	}

	handlePlayPause = () => {
		this.setState((prevState) => {
			return {
				paused: !prevState.paused,
				showPlayPause: true
			}
		});
		setTimeout(() => {
			this.setState({
				showPlayPause: (this.state.paused) ? true : false
			});
		}, 3000)
	}

	handleFullscreen = () => {
		this.player.presentFullscreenPlayer()
	}

	handleVolume = () => {
		this.setState((prevState) => {
			return { muted: !prevState.muted }
		});
	}

	render() {
		return (
			<LayoutVideo
				video={
					<Video source={{ uri: 'https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4' }}   // Can be a URL or a local file.
						ref={(ref) => {
							this.player = ref
						}}
						style={styles.video}
						resizeMode="contain"
						onBuffer={this.onBuffer}
						onLoad={this.onLoad}
						paused={this.state.paused}
						muted={this.state.muted}
						onTimedMetadata={this.setTime}
						onProgress={this.setProgress}
					/>
				}
				loader={<ActivityIndicator color="#00b92f" size="large" />}
				isLoading={this.state.isLoading}
				playPause={<PlayPause handlePlayPause={this.handlePlayPause} paused={this.state.paused} showPlayPause={this.state.showPlayPause} />}
				controls={
					<ControlsLayout>
						<TimeVideo duration={formattedTime(this.state.duration)} progress={formattedTime(this.state.currentTime)} />
						<ProgressBar progress={this.state.progress} />
						<Volume handleVolume={this.handleVolume} isMute={this.state.muted} />
						<Fullscreen handleFullscreen={this.handleFullscreen} />
					</ControlsLayout>
				} />
		)
	}
}
