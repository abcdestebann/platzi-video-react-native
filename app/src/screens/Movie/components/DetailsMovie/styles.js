import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
   trailer: {
      height: 300,
      marginBottom: 20,
   },
   details: {
      flexDirection: 'row',
      marginBottom: 20,
      padding: 20,
      flex: 1,
   },
   image: {
      width: 125,
      height: 190,
   },
   title: {
      color: '#44546b',
      fontSize: 18,
      textAlign: 'center',
      fontWeight: 'bold',
   },
   top: {
      borderBottomWidth: 1,
      borderBottomColor: '#eaeaea',
      padding: 20,
      backgroundColor: 'white',
   },
   bottom: {

   },
   description: {
      fontSize: 15,
      lineHeight: 22.5,
      color: '#4c4c4c',
      marginLeft: 10,
      flex: 1,
   }
})