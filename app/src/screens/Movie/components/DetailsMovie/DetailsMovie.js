import React from 'react';
import { View, ScrollView, Text, Image, WebView } from 'react-native';
import styles from './styles';


const makeHtml = (id) => {
   return (`
      <style>
         iframe {
            position: absolute;
            left: 0;
            top: 0;
            right: 0;
            bottom 0;
            width: 100%;
            height: 100%;
            margin-bottom: 10px;
         }
         .video {
            position: relative;
            padding-bottom: 56.25%;
            margin-bottom: 10px;

         }
      </style>
      <div class="video">
         <iframe src="https://www.youtube.com/embed/${id}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
   `)
}


const DetailsMovie = ({ title, summary, large_cover_image, yt_trailer_code }) => (
   <ScrollView>
      <View style={styles.top}>
         <Text style={styles.title}>{title}</Text>
      </View>
      <View style={styles.details}>
         <Image
            source={{ uri: large_cover_image }}
            style={styles.image} />
         <Text style={styles.description}> {summary} </Text>
      </View>
      <View style={styles.trailer}>
         <WebView originWhitelist={['*']} source={{ html: makeHtml(yt_trailer_code) }} />
      </View>
   </ScrollView>

);

export default DetailsMovie;
