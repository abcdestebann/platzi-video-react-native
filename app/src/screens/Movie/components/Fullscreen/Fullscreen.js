import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'

const Fullscreen = ({ handleFullscreen }) => (
   <TouchableOpacity
      onPress={handleFullscreen}
      hitSlop={{
         top: 5,
         left: 5,
         right: 5,
         bottom: 5
      }}>
      <Icon name="fullscreen" size={30} color="white" />
   </TouchableOpacity>
);

export default Fullscreen;
