import React from 'react';
import { View } from 'react-native';

const LayoutMovie = ({ children }) => (
   <View >
      {children}
   </View>
);

export default LayoutMovie;
