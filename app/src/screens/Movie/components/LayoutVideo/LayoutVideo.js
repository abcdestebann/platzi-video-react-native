import React from 'react';
import { View } from 'react-native';
import styles from './styles'

const LayoutVideo = ({ video, loader, isLoading, controls, playPause }) => (
    <View style={styles.container}>
        <View style={styles.video}>
            {video}
        </View>
        {playPause}
        {isLoading && <View style={styles.overlay}>
            {loader}
        </View>}
        {controls}
    </View>
);

export default LayoutVideo;
