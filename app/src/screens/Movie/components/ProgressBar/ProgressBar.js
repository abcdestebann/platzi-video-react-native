import React from 'react';
import {
   ProgressBarAndroid,
   ProgressViewIOS,
   Platform,
   StyleSheet
} from 'react-native';

const ProgressBar = (props) => {

   return (
      Platform.select({
         ios: <ProgressViewIOS style={styles.progresBar} progressTintColor='white' trackTintColor='rgba(0, 0, 0, 0.3)' progress={props.progress} />,
         android: <ProgressBarAndroid styleAttr="Horizontal" indeterminate={false} style={styles.progresBar} progress={props.progress} />
      })
   )
}

const styles = StyleSheet.create({
   progresBar: {
      width: Platform.select({
         ios: 200,
         android: 250
      }),
      marginHorizontal: Platform.select({
         ios: 10,
         android: 10
      }),
   }
})

export default ProgressBar