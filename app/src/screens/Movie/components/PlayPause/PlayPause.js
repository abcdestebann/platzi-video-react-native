import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'

const PlayPause = ({ handlePlayPause, paused, showPlayPause }) => (
	<TouchableOpacity
		onPress={handlePlayPause}
		style={styles.playpause}
		hitSlop={{
			top: 5,
			left: 5,
			right: 5,
			bottom: 5
		}}>
		{
			(showPlayPause) ? (paused) ? <Icon name="play-arrow" size={60} color="white" /> : <Icon name="pause" size={60} color="white" /> : null
		}
	</TouchableOpacity>
);

export default PlayPause;


const styles = StyleSheet.create({
	playpause: {
		position: 'absolute',
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		justifyContent: 'center',
		alignItems: 'center',
	}
});