import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

const TimeVideo = ({ duration, progress }) => (
   <View>
      <Text style={styles.time}> {progress} / {duration} </Text>
   </View>
);

export default TimeVideo;
