import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
   time: {
      fontSize: 10,
      color: 'white',
      fontWeight: 'bold',
   }
})