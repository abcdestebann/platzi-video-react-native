import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'

const Volume = ({ handleVolume, isMute }) => (
   <TouchableOpacity
      onPress={handleVolume}
      hitSlop={{
         top: 5,
         left: 5,
         right: 5,
         bottom: 5
      }}>
      {isMute ? <Icon name="volume-off" size={30} color="white" /> : <Icon name="volume-up" size={30} color="white" />}
   </TouchableOpacity>
);

export default Volume;
