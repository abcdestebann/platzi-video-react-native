import React from 'react';
import { View } from 'react-native';
import styles from './styles'

const ControlsLayout = ({ children }) => (
    <View style={styles.container}>
        {children}
    </View>
);

export default ControlsLayout;
