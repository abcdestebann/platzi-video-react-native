import React, { Component } from 'react'
import { View } from 'react-native';

import { connect } from 'react-redux';

import Header from '../../components/Header/Header';
import SuggestionList from './containers/SuggestionList/SuggestionList';
import CategoryList from './containers/CategoryList/CategoryList';
import Search from './containers/Search/Search';

class Home extends Component {
	render() {
		console.log(this.props)
		if (this.props.movie && Object.keys(this.props.movie).length == 0) {
			return (
				<View>
					<Header />
					<Search />
					<CategoryList categories={this.props.categories} />
					<SuggestionList suggestions={this.props.suggestions} />
				</View>
			)
		}
		return (<View></View>)
	}
}

const mapStateToProps = (state) => {
	console.log(state)
	return {
		categories: state.categories,
		suggestions: state.suggestions,
		movie: state.movie
	}
}

export default connect(mapStateToProps)(Home)

