import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 5,
    },
    imageContainer: {

    },
    image: {
        width: 100,
        height: 150,
    },
    info: {
        padding: 10,
        justifyContent: 'space-around',
    },
    title: {
        fontSize: 22,
        color: '#44546B',
    },
    author: {
        backgroundColor: '#8BC34A',
        maxWidth: '100%',
        alignSelf: 'flex-start',
        color: 'white',
        paddingVertical: 4,
        paddingHorizontal: 6,
        fontSize: 12,
        borderRadius: 5,
        overflow: 'hidden',
    },
    views: {
        backgroundColor: '#FFD54F',
        maxWidth: '100%',
        alignSelf: 'flex-start',
        color: 'white',
        paddingVertical: 4,
        paddingHorizontal: 6,
        fontSize: 10,
        borderRadius: 5,
        overflow: 'hidden',
    },
})