import React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import styles from './styles';


const Suggestion = (props) => (
    <TouchableOpacity style={styles.container} onPress={() => { props.handleShowMovie(props) }}>
        <View style={styles.imageContainer}>
            <Image
                source={{ uri: props.medium_cover_image }}
                style={styles.image} />
        </View>
        <View style={styles.info}>
            <Text style={styles.title}>{props.title}</Text>
            <Text style={styles.author}>{props.year}</Text>
            <Text style={styles.views}>{props.rating}</Text>
        </View>
    </TouchableOpacity>
);

export default Suggestion;
