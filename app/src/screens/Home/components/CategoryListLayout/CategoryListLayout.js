import React from 'react';
import { View, Text, ImageBackground } from 'react-native';
import styles from './styles';

const CategoryListLayout = ({ children, title }) => (
    <ImageBackground
        source={require('../../../../../assets/backgronund.png')}
        style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        {children}
    </ImageBackground>
);

export default CategoryListLayout;
