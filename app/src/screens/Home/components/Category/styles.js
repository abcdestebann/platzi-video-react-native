import { StyleSheet, Platform } from 'react-native';

export default styles = StyleSheet.create({
    imageContain: {
        width: 250,
        height: 100,
        resizeMode: 'contain',
        marginLeft: 10,
        borderRadius: 10,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        textShadowColor: 'rgba(0, 0, 0, .8)',
        textShadowOffset: { width: 1, height: 3 },
        textShadowRadius: 5,
        elevation: 10
    }
})