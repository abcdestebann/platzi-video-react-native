import React from 'react';
import { View, Text, ImageBackground } from 'react-native';
import styles from './styles';

const Category = (props) => (
    <ImageBackground
        source={{ uri: props.background_image }}
        style={styles.imageContain}>
        <Text style={styles.text}>{props.genres[1]}</Text>
    </ImageBackground>
);

export default Category;
