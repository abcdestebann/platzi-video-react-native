import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    container: {
        paddingVertical: 30,
        paddingHorizontal: 10,
    },
    title: {
        color: '#4C4C4C',
        fontSize: 20,
        marginBottom: 10,
        marginLeft: 8,
        fontWeight: 'bold',
    }
});