import React, { Component } from 'react'
import { FlatList, Text, View } from 'react-native';

import { connect } from 'react-redux'
import { setMovieToState } from '../../../../actions/index';

import SuggestionListLayout from '../../components/SuggestionListLayout/SuggestionListLayout';
import Suggestion from '../../components/Suggestion/Suggestion'
import EmptyList from '../../../../components/EmptyList/EmptyList'
import ItemSeparator from '../../../../components/ItemSeparator/ItemSeparator';

class SuggestionList extends Component {

	state = {
		titleEmpty: 'No hay recomendados :)'
	}

	
	renderEmptyList = () => <EmptyList title={this.state.titleEmpty} />
	
	renderItemSeparator = () => <ItemSeparator />

	handleShowMovie = (item) => {
		console.log(item)
		console.log(this.props)
		this.props.setMovieToState(item)
	} 

	renderItem = ({ item }) => <Suggestion {...item} handleShowMovie={this.handleShowMovie} />

	keyExtractor = (item) => String(item.id)

	render() {
		return (
			<View>
				<SuggestionListLayout title="Recomendado para ti">
					<FlatList
						keyExtractor={this.keyExtractor}
						data={this.props.suggestions}
						ListEmptyComponent={this.renderEmptyList}
						ItemSeparatorComponent={this.renderItemSeparator}
						renderItem={this.renderItem}
					/>
				</SuggestionListLayout>
			</View>
		)
	}
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = (dispatch) => {
	return {
		setMovieToState: (data) => dispatch(setMovieToState(data))
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(SuggestionList)