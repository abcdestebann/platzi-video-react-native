import React, { Component } from 'react';
import { TextInput, StyleSheet } from 'react-native';
import { connect } from 'react-redux'
import { setMovieToState } from '../../../../actions/index';
import api from '../../../../api/index';


class Search extends Component {

   state = {
      text: ''
   }

   handleSubmit = () => {
      api.searchMovie(this.state.text).then((data) => this.props.setMovieToState(data.movies[0]))
   }

   handleChangeText = (text) => {
      this.setState({ text })
   }

   render() {
      return (
         <TextInput
            placeholder="Busca tu película favorita"
            autoCorrect={false}
            autoCapitalize="none"
            underlineColorAndroid="transparent"
            onSubmitEditing={this.handleSubmit}
            onChangeText={this.handleChangeText}
            style={styles.input}
         />
      );
   }
}

const styles = StyleSheet.create({
   input: {
      padding: 15,
      fontSize: 15,
      borderBottomColor: '#eaeaea',
      borderBottomWidth: 2
   }
});

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => {
   return {
      setMovieToState: (data) => dispatch(setMovieToState(data))
   }
}


export default connect(mapStateToProps, mapDispatchToProps)(Search)
