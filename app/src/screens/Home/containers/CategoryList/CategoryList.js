import React, { Component } from 'react'
import { Text, View, FlatList } from 'react-native'

import CategoryListLayout from '../../components/CategoryListLayout/CategoryListLayout';
import Category from '../../components/Category/Category'
import EmptyList from '../../../../components/EmptyList/EmptyList'
import ItemSeparator from '../../../../components/ItemSeparator/ItemSeparator';

export default class CategoryList extends Component {


    renderEmptyList = () => <EmptyList title="No hay categorias :(" />

    renderItemSeparator = () => <ItemSeparator />

    renderItem = ({ item }) => <Category {...item} />

    keyExtractor = (item) => String(item.id)

    render() {
        return (
            <View>
                <CategoryListLayout title="Categorias">
                    <FlatList
                        horizontal
                        keyExtractor={this.keyExtractor}
                        data={this.props.categories}
                        ListEmptyComponent={this.renderEmptyList}
                        ItemSeparatorComponent={this.renderItemSeparator}
                        renderItem={this.renderItem}
                    />
                </CategoryListLayout>
            </View>
        )
    }
}