import {
   SET_DATA_CATEGORIES,
   SET_DATA_SUGGESTIONS,
   SET_MOVIE_SELECTED,
   SET_NULL_MOVIE
} from './constants';


export const setCategoriesToState = (payload) => ({
   type: SET_DATA_CATEGORIES,
   payload: {
      categories: payload
   }
})


export const setSuggesitonsToState = (payload) => ({
   type: SET_DATA_SUGGESTIONS,
   payload: {
      suggestions: payload
   }
})

export const setMovieToState = (payload) => ({
   type: SET_MOVIE_SELECTED,
   payload: {
      movie: payload
   }
})


