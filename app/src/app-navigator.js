import { createStackNavigator } from 'react-navigation';

import LoadingLogo from './components/LoadingLogo/LoadingLogo'

const Main = createStackNavigator(
   {
      Home: LoadingLogo
   }
)

export default Main