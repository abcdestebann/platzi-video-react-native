import axios from 'axios';

import { ENDPOINT, ROUTES } from './constants'

class Api {

    constructor() { }

    /**
     *
     *
     * @param {*} id
     * @returns
     * @memberof Api
     */
    async getSuggestions(id) {
        const url = `${ENDPOINT}${ROUTES.SUGGESTIONS}${id}`
        const { data } = await axios.get(url)
        return data.data
    }

    /**
     *
     *
     * @returns
     * @memberof Api
     */
    async getCategories() {
        const url = `${ENDPOINT}${ROUTES.CATEGORIES}`
        const { data } = await axios.get(url)
        console.log(data)
        return data.data
    }

    /**
     *
     *
     * @param {*} title
     * @returns
     * @memberof Api
     */
    async searchMovie(title) {
        const url = `${ENDPOINT}${ROUTES.CATEGORIES}?limit=1&sort_by=rating&query_term=${title}`
        const { data } = await axios.get(url)
        console.log(data)
        return data.data
    }


}

export default new Api()