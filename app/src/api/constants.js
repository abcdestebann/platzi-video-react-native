export const ENDPOINT = 'https://yts.am/api/v2/'

export const ROUTES = {
    SUGGESTIONS: 'movie_suggestions.json?movie_id=',
    CATEGORIES: 'list_movies.json'
}