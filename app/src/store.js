import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import reducer from './reducers/index';

const persistConfig = {
   key: 'root',
   storage,
   blacklist: ['movie']
}

const persistedReducer = persistReducer(persistConfig, reducer)

export default function configureStore() {
   const store = createStore(persistedReducer, {})
   const persistor = persistStore(store)
   return { store, persistor }
}